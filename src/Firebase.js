import * as firebase from 'firebase';
// import firestore from 'firebase/firestore'

const settings = {timestampsInSnapshots: true};

const firebaseConfig = {
  apiKey: "AIzaSyA5Xj6PW3upt0T8F3WMN7o-VLr9UG9s-Gk",
  authDomain: "reactapp-ac77d.firebaseapp.com",
  databaseURL: "https://reactapp-ac77d.firebaseio.com",
  projectId: "reactapp-ac77d",
  storageBucket: "reactapp-ac77d.appspot.com",
  messagingSenderId: "87535783139",
  
};

firebase.initializeApp(firebaseConfig);

firebase.firestore().settings(settings);
// const storage = firebase.storage();

export default firebase;
