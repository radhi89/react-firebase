import React, {Component} from 'react';
import firebase from '../Firebase';
import { Link } from 'react-router-dom';




class Home extends Component{

   constructor(props){
    super(props);

this.ref = firebase.firestore().collection('Users');
    this.unsubscribe = null;
    this.state = {
      boards: [],

    };
  }

 onCollectionUpdate = (querySnapshot) => {
    const boards = [];
    querySnapshot.forEach((doc) => {
      const { name, address, gender } = doc.data();
      boards.push({
        key: doc.id,
        doc, // DocumentSnapshot
        name,
        address,
        gender,
        });
    });
    this.setState({
      boards
   });
  }

  componentDidMount() {

    this.unsubscribe = this.ref.onSnapshot(this.onCollectionUpdate);
  }
  




render(){
  return (

  <div>
      

      

  <div class="container">
        <div class="panel panel-default"> 
          <div class="panel-heading">
            <h3 class="panel-title">
              USER LIST
            </h3>
          </div>
          <div class="panel-body">
            <h4 style={{marginRight: 100 + 'em'}} ><Link to="/create">+User</Link></h4>
            <table class="table table-stripe">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Address</th>
                  <th>Gender</th>
               
                </tr>
              </thead>
              <tbody>
                {this.state.boards.map(board =>
                  <tr>
                    <td><Link to={`/show/${board.key}`}>{board.name}</Link></td>
                    <td>{board.address}</td>
                    <td>{board.gender}</td>
                  
                  </tr>
                )}
              </tbody>
            </table>
          </div>
        </div>
</div>
</div>
    )
}

  
}


export default Home;