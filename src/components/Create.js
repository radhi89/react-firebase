import React, { Component } from 'react';
import firebase from '../Firebase';
import { Link } from 'react-router-dom';

class Create extends Component {

  constructor() {
    super();
    this.ref = firebase.firestore().collection('Users');
    this.state = {
      name: '',
      address: '',
      gender:''

    };
  }
  onChange = (e) => {
    const state = this.state
    state[e.target.name] = e.target.value;
    this.setState(state);
  }

  onSubmit = (e) => {
    e.preventDefault();

    const { name, address,gender} = this.state;

    this.ref.add({
      name,
      address,
      gender,
   
    }).then((docRef) => {
      this.setState({
        name: '',
        address: '',
        gender: ''
       
      });
      this.props.history.push("/")
    })
    .catch((error) => {
      console.error("Error adding user: ", error);
    });
  }

  render() {
    const { name, address, gender } = this.state;
    return (
      <div class="container">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 style={{font: 'times new roman' , textAlign: 'center' }}class="panel-title">
              ADD USER
            </h3>
          </div>
          <div class="panel-body">
            <h4><Link to="/" class="btn btn-primary">Back to Home</Link></h4>
            <form onSubmit={this.onSubmit}>
              <div class="form-group">
                <label for="name">Name:</label>
                <input type="text" class="form-control" name="name" value={name} onChange={this.onChange} placeholder="Name.." />
              </div>
              <div class="form-group">
                <label for="address">Address:</label>
                <textArea class="form-control" name="address" onChange={this.onChange} placeholder="Address" cols="80" rows="3">{address}</textArea>
              </div>
              <div class="form-group">
              <label for="gender">Gender:</label>
              <input type="text" class="form-control" name="gender" value={gender} onChange={this.onChange} placeholder="Gender" />
              </div>
              
              <button type="submit" class="btn btn-success">Submit</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default Create;