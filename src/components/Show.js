import React, { Component } from 'react';
import firebase from '../Firebase';
import { Link } from 'react-router-dom';

class Show extends Component {

  constructor(props) {
    super(props);
    this.state = {
      users: [],
      key: ''
    };
  }

  componentDidMount() {
 
    const ref = firebase.firestore().collection('Users').doc(this.props.match.params.id);
    ref.get().then((doc) => {
      if (doc.exists) {
        const user = doc.data();
        this.setState({      
          key: doc.id,
          name: user.name,
          address: user.address,
          gender: user.gender,
 
              isLoading: false
        });
      } else {
        console.log("No such user!");
      }
    });
  }

  delete(id){
    firebase.firestore().collection('Users').doc(id).delete().then(() => {
      console.log("User successfully deleted!");
      this.props.history.push("/")
    }).catch((error) => {
      console.error("Error removing User: ", error);
    });
  }

  render() {
    return (
      <div class="container">
        <div class="panel panel-default">
          <div class="panel-heading">
          <h4><Link to="/" class="btn btn-primary">Back to Home</Link></h4>
            <h3 class="panel-title">
              {this.state.name}
            </h3>
          </div>
          <div class="panel-body">
            <dl>
              <dt>Gender:</dt>
              <dd>{this.state.gender}</dd>
              <dt>Address:</dt>
              <dd>{this.state.address}</dd>
           
            </dl>
            <Link to={`/edit/${this.state.key}`} class="btn btn-success">Edit</Link>&nbsp;
            <button onClick={this.delete.bind(this, this.state.key)} class="btn btn-danger">Delete</button>
          </div>
        </div>
      </div>
    );
  }
}

export default Show;