import React,{Component} from 'react';
import firebase from '../Firebase';


class Signup extends Component{
	constructor(props){
		super(props);
		this.handleChange = this.handleChange.bind(this);
		this.signup = this.signup.bind(this);
		this.state = {
			email : '',
			password : '',
		}

	}



signup(e){

	e.preventDefault();
	firebase.auth().createUserWithEmailAndPassword(this.state.email, this.state.password).catch((error) => {
		console.log(error);
	})
	
}


handleChange(e){
	
	this.setState({[e.target.name]: e.target.value });
}
 
render(){
	return(





<div>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"/>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"/>


<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"/>


<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"/>

	<form>
		<div className="form-group">
			<label>Email Address: </label>
			<input value={this.state.email} onChange={this.handleChange} type="email" name="email"
			className="form-control" placeholder="Enter mail id.." />

		</div>

		<div className="form-group">
			<label>Password : </label>
			<input value={this.state.password} onChange={this.handleChange} type="password" name="password"
			className="form-control" placeholder="Enter Password.." />

		</div>
		
		<button onClick={this.signup} style={{marginLeft: '25px'}} className="btn btn-success">Sign Up</button>
	</form>
	 
</div>


		)

	}
}





export default Signup;
