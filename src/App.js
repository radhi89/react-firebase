import React, { Component } from 'react';
import './App.css';
import firebase from './Firebase';
import UserIcon from '@material-ui/icons/People';
import { UserList, UserShow, UserCreate, UserEdit } from "./components/Users";
import { PostList, PostShow, PostCreate, PostEdit } from "./posts";
import { FirebaseDataProvider, FirebaseAuthProvider } from 'react-admin-firebase';
import { Admin, Resource} from "react-admin";
import LoginPage from './LoginPage';


const firebaseConfig = {
  apiKey: "AIzaSyA5Xj6PW3upt0T8F3WMN7o-VLr9UG9s-Gk",
  authDomain: "reactapp-ac77d.firebaseapp.com",
  databaseURL: "https://reactapp-ac77d.firebaseio.com",
  projectId: "reactapp-ac77d",
  storageBucket: "reactapp-ac77d.appspot.com",
  messagingSenderId: "87535783139",
 appId: "1:87535783139:web:0abfbd05b2e0b1b0aec9d6"
};



const options = {
  logging: false,
  // rootRef: 'rootrefcollection/QQG2McwjR2Bohi9OwQzP',
 appId: "1:87535783139:web:0abfbd05b2e0b1b0aec9d6",
  // watch: ['posts'];
  // dontwatch: ['comments'];
  persistence: 'local',
  // disableMeta: true
  dontAddIdFieldToDoc: true
}



const authProvider = FirebaseAuthProvider(firebaseConfig, options);
const dataProvider = FirebaseDataProvider(firebaseConfig,options);


class App extends Component{



constructor(props){
  super(props);

  this.state = {
    user : [],
    posts :[],

  }
}

  

logout(){
  firebase.auth().signOut();
}


componentDidMount(){
  this.authListener();
}


authListener(){
  firebase.auth().onAuthStateChanged((user) => {
    //console.log(user)
    if (user){
      this.setState({user});
      //localStorage.setItem('user', user.uid);

    }
    else{
      this.setState({ user: null });
      //localStorage.removeItem('user');
    }

  })
}
render(){
  
  return (
    
<div className="App">
      <Admin  loginPage={LoginPage} dataProvider={dataProvider} authProvider={authProvider}>
      <Resource name="posts" list={PostList} edit={PostEdit} create={PostCreate} show={PostShow}/>
     <Resource  name="Users" icon={UserIcon} list={UserList}  show={UserShow} create={UserCreate}
          edit={UserEdit}
        />
      </Admin>
         
    </div>

      );
}

}
      
export default App;

